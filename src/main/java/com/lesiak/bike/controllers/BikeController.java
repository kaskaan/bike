package com.lesiak.bike.controllers;

import com.lesiak.bike.models.Bike;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/v1/bikes")
public class BikeController {

    @GetMapping
    public List<Bike> getListOfBikes() {
        List<Bike> bikesList = new ArrayList<>();
        return bikesList;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    public void createBike(@RequestBody Bike bike) {

    }

    @GetMapping("/{id}")
    public Bike getBike(@PathVariable long id) {
        return new Bike();
    }

}
